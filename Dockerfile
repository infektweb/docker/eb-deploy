FROM node:16-alpine

RUN apk add --no-cache \
        python3 \
        python3-dev \
        gcc \
        musl-dev \
        libffi-dev \
        make \
        py3-pip \
        unzip \
        curl \
        git \
        jq \
    && pip3 install --upgrade pip \
    && pip3 install wheel \
    && pip3 install awsebcli --upgrade --ignore-installed \
    && pip3 install --no-cache-dir \
        awscli

RUN rm -rf /var/cache/apk/*
